package com.crea.leo.MusicXChocolate.Model;

import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Field;

@Document(indexName = "leo", type = "music")
public class Music {
    @Id
    @Field(type = FieldType.Integer)
    private int id;

    @Field(type = FieldType.Text)
    private String songName;

    @Field(type = FieldType.Text)
    private String artist;

    @Field(type = FieldType.Text)
    private String album;

    @Field(type = FieldType.Text)
    private String style;

    @Field(type = FieldType.Boolean)
    private boolean favourite;

    public Music() {
    }

    public Music(int id, String songName, String artist, String album, String style, boolean favourite) {
        this.id = id;
        this.songName = songName;
        this.artist = artist;
        this.album = album;
        this.style = style;
        this.favourite = favourite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }
}