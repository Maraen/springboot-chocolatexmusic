package com.crea.leo.MusicXChocolate.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Chocolate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private boolean expensive;
    private boolean singleBlend;
    private int CacaoPercent;
    private String description;

    public Chocolate() {
    }

    public Chocolate(int id, String name, boolean expensive, boolean singleBlend, int cacaoPercent,
            String description) {
        this.id = id;
        this.name = name;
        this.expensive = expensive;
        this.singleBlend = singleBlend;
        CacaoPercent = cacaoPercent;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isExpensive() {
        return expensive;
    }

    public void setExpensive(boolean expensive) {
        this.expensive = expensive;
    }

    public boolean isSingleBlend() {
        return singleBlend;
    }

    public void setSingleBlend(boolean singleBlend) {
        this.singleBlend = singleBlend;
    }

    public int getCacaoPercent() {
        return CacaoPercent;
    }

    public void setCacaoPercent(int cacaoPercent) {
        CacaoPercent = cacaoPercent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}