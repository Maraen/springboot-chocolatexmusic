package com.crea.leo.MusicXChocolate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicXChocolateApplication {

	public static void main(String[] args) {
		SpringApplication.run(MusicXChocolateApplication.class, args);
	}

}
