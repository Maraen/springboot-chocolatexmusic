package com.crea.leo.MusicXChocolate.Controller;

import java.util.List;
import com.crea.leo.MusicXChocolate.Model.Chocolate;

import com.crea.leo.MusicXChocolate.DAO.ChocolateDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class ChocolateController {

    @Autowired
    private ChocolateDAO chocolateDAO;

    @RequestMapping(value = "/Chocolates", method = RequestMethod.GET)
    public List<Chocolate> listChocolates() {
        return chocolateDAO.findAll();
    }

    @RequestMapping(value = "/Chocolates/{id}", method = RequestMethod.GET)
    public String showSpecificChocolate(@PathVariable int id) {
        Chocolate chocolateID = chocolateDAO.findByID(id);
        return chocolateID.toString();
    }

    @RequestMapping(value = "/ChocolatesGetByName/{name}")
    public Chocolate showChocolateByName(@PathVariable String name) {
        return chocolateDAO.findByNameLike(name);
    }

    @RequestMapping(value = "/Chocolates/SingleBlend", method = RequestMethod.GET)
    public List<Chocolate> showOnlySingleBlend() {
        List<Chocolate> chocolateSingleBlend = chocolateDAO.findAllSingleblend();
        return chocolateSingleBlend;
    }

    @RequestMapping(value = "/addChocolate", method = RequestMethod.POST)
    public Chocolate addNew(@RequestBody Chocolate chocolate) {
        return chocolateDAO.save(chocolate);
    }

    @RequestMapping(value = "/updateChocolate/{id}", method = RequestMethod.PUT)
    public Chocolate updateNew(@RequestBody Chocolate chocolate) {
        return chocolateDAO.save(chocolate);
    }

    @RequestMapping(value = "/deleteChocolate/{id}", method = RequestMethod.DELETE)
    public List<Chocolate> DeleteSpecificChocolate(@PathVariable int id) {
        chocolateDAO.deleteById(id);
        return chocolateDAO.findAll();
    }

}