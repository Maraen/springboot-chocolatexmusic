package com.crea.leo.MusicXChocolate.Controller;

import java.util.List;
import com.crea.leo.MusicXChocolate.Model.Music;

import com.crea.leo.MusicXChocolate.DAO.MusicDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class MusicController {

    @Autowired
    private MusicDAO musicDAO;

    @RequestMapping(value = "/Musics", method = RequestMethod.GET)
    public List<Music> listMusics() {
        return musicDAO.findAll();
    }

    @RequestMapping(value = "/Musics/{id}", method = RequestMethod.GET)
    public String showSpecificMusic(@PathVariable int id) {
        Music musicID = musicDAO.findByID(id);
        return musicID.toString();
    }

    @RequestMapping(value = "/MusicsGetByName/{name}")
    public Music showMusicByName(@PathVariable String songName) {
        return musicDAO.findByNameLike(songName);
    }

    @RequestMapping(value = "/MusicsGetByArtist/{name}")
    public Music showMusicByArtist(@PathVariable String artist) {
        return musicDAO.findByArtistNameLike(artist);
    }

    @RequestMapping(value = "/addMusic", method = RequestMethod.POST)
    public Music addNew(@RequestBody Music music) {
        return musicDAO.save(music);
    }

    @RequestMapping(value = "/updateMusic/{id}", method = RequestMethod.PUT)
    public Music updateNew(@RequestBody Music music) {
        return musicDAO.save(music);
    }

    @RequestMapping(value = "/deleteMusic/{id}", method = RequestMethod.DELETE)
    public List<Music> DeleteSpecificSong(@PathVariable int id) {
        musicDAO.deleteById(id);
        return musicDAO.findAll();
    }

}