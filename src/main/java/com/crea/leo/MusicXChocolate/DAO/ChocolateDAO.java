package com.crea.leo.MusicXChocolate.DAO;

import java.util.List;

import com.crea.leo.MusicXChocolate.Model.Chocolate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChocolateDAO extends JpaRepository<Chocolate, Integer> {

    public List<Chocolate> findAll();

    public Chocolate findByID(int id);

    public List<Chocolate> findAllExpensive();

    public List<Chocolate> findAllSingleblend();

    public Chocolate findByNameLike(String name);
}