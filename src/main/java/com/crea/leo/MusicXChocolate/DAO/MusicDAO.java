package com.crea.leo.MusicXChocolate.DAO;

import java.util.List;

import com.crea.leo.MusicXChocolate.Model.Music;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MusicDAO extends JpaRepository<Music, Integer> {

    public List<Music> findAll();

    public Music findByID(int id);

    public Music findByArtistNameLike(String artist);

    public Music findByNameLike(String songName);
}