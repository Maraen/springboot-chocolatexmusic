INSERT INTO chocolate
    (id, name, expensive, singleBlend, CacaoPercent, description)
VALUES(1, 'Milk Chocolate', FALSE, FALSE, 33, "The most basic and most sold type of chocolate all around the world.");

INSERT INTO chocolate
    (id, name, expensive, singleBlend, CacaoPercent, description)
VALUES(2, 'Dark Chocolate', TRUE, FALSE, 70, "Dark Chocolate, a chocolate type containing no milk and clearly the superior version of chocolate.");

INSERT INTO chocolate
    (id, name, expensive, singleBlend, CacaoPercent, description)
VALUES(3, 'White Chocolate', TRUE, FALSE, 0, 'The worst type of "chocolate" ever created, way too sweet even for me and I like redbull.');

INSERT INTO chocolate
    (id, name, expensive, singleBlend, CacaoPercent, description)
VALUES(4, 'Ruby Chocolate', TRUE, TRUE, null, 'Many people consider this the 4th type of chocolate, made by Barry Callebaut, this chocolates taste is out of this world.');
